package org.springSuite.mvc.test.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
//import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import es.springsuite.mvc.dao.common.IEstadosDao;

@RunWith(SpringRunner.class)
//@SpringBootTest
//@TestPropertySource(locations = "classpath:db-test.properties")
public class EstadosDaoTest {

	@Autowired
	private IEstadosDao estadosDAO;
	
    @Test
    public void contextLoads() {
    	assertNotNull(estadosDAO);
    	
    }
}
