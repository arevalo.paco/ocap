package es.springsuite.mvc.dao.common.impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.common.IGruposCategoriasDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.common.GruposCategoriasEntity;

/**
 * Data access object for entity GruposCategoriasEntity
 * 
 * @author farevalo
 */
@Repository
public class GruposCategoriasDaoImpl extends GenericDaoImpl<GruposCategoriasEntity, Long> implements IGruposCategoriasDao {

}
