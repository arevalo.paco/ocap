package es.springsuite.mvc.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

public interface IGenericDao<T, ID extends Serializable> {

	T insert(T entity);
	
	T patch(T entity);
	
	void delete(ID id);
	
	T findById(ID id);
	
	List<T> findAll();
	
	List<?> findByCriteria(Criteria criteria);
}
