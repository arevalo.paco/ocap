package es.springsuite.mvc.dao.estatutos.Impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.estatutos.IEstatutarioDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.estatutos.EstatutarioEntity;

@Repository
public class EstatutarioDaoImpl extends GenericDaoImpl<EstatutarioEntity, Long> 
    implements IEstatutarioDao {

}
