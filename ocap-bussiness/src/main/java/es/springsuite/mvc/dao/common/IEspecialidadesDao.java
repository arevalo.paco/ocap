package es.springsuite.mvc.dao.common;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.common.EspecialidadesEntity;

public interface IEspecialidadesDao extends IGenericDao<EspecialidadesEntity, Long> {

}
