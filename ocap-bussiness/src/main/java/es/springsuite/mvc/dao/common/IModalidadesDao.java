package es.springsuite.mvc.dao.common;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.common.ModalidadesEntity;

public interface IModalidadesDao extends IGenericDao<ModalidadesEntity, Long> {

}
