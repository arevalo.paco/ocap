package es.springsuite.mvc.dao.usuarios;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.usuarios.UsuariosEntity;

public interface IUsuariosDao extends IGenericDao<UsuariosEntity, Long> {

	UsuariosEntity checkUsuario(String usuario, String password);
}
