package es.springsuite.mvc.dao.common.impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.common.IEspecialidadesDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.common.EspecialidadesEntity;

@Repository
public class EspecialidadesDaoImpl 
	extends GenericDaoImpl<EspecialidadesEntity, Long> 
	implements IEspecialidadesDao {

}
