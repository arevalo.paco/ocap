package es.springsuite.mvc.dao.common;

import java.util.List;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.common.CategoriasProfesionalesEntity;

public interface ICategoriasProfesionalesDao extends IGenericDao<CategoriasProfesionalesEntity, Long> {

	List<?> listadoPrueba();

}
