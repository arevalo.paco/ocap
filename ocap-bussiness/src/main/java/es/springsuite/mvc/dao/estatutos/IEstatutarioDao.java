package es.springsuite.mvc.dao.estatutos;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.estatutos.EstatutarioEntity;

public interface IEstatutarioDao extends IGenericDao<EstatutarioEntity, Long> {

}
