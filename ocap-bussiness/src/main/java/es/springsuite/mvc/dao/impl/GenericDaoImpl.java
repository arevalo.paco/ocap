package es.springsuite.mvc.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import es.springsuite.mvc.dao.IGenericDao;

/**
 * Generic class DAO
 * @author farevalo
 *
 * @param <T>
 * @param <ID>
 */
public class GenericDaoImpl<T, ID extends Serializable>  
extends HibernateTemplate
implements IGenericDao<T, ID> {  

	private Class<T> persistentClass;
	
	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	@Autowired
	public void setSessionFactory(SessionFactory s) {
 		super.setSessionFactory(s);
	}
	
	protected Session getSession() {
		return getSessionFactory().	openSession();
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	public T findById(ID id, boolean lock) {
		T entity;
		if (lock)
			entity = (T) getSession().load(getPersistentClass(), id, LockMode.PESSIMISTIC_WRITE);
		else
			entity = (T) getSession().load(getPersistentClass(), id);

		return entity;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<T> findAll() {
		Session session = getSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<T> query = cb.createQuery(this.persistentClass);
		Root<T> rootEntry = query.from(this.persistentClass);
		query.select(rootEntry);

		TypedQuery<?> allQuery = session.createQuery(query);
		return (List<T>) allQuery.getResultList();
	}

	public T makePersistent(T entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	public void makeTransient(T entity) {
		getSession().delete(entity);
	}

	public void flush() {
		getSession().flush();
	}

	public void clear() {
		getSession().clear();
	}

	/** 
	* Use this inside subclasses as a convenience method. 
	*/  
	@SuppressWarnings({ "unchecked", "deprecation" })  
	protected List<T> findByCriteria(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}

	@Override
	public T insert(T entity) {
		getSession().save(entity);
		
		return entity;
	}

	@Override
	public T patch(T entity) {
		getSession().saveOrUpdate(entity);
		
		return null;
	}

	@Override
	public void delete(ID id) {
		getSession().remove(id);
	}

	@Override
	public T findById(ID id) {
		return getSession().get(persistentClass, id);
	}

	@Override
	public List<?> findByCriteria(Criteria criteria) {
		return criteria.list();
	}

} 