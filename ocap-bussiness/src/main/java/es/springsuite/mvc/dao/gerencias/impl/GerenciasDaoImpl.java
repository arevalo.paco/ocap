package es.springsuite.mvc.dao.gerencias.impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.gerencias.IGerenciasDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.gerencias.GerenciasEntity;

@Repository
public class GerenciasDaoImpl 
    extends GenericDaoImpl<GerenciasEntity, Long>
    implements IGerenciasDao {

}
