package es.springsuite.mvc.dao.common.impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.common.IModalidadesDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.common.ModalidadesEntity;

@Repository
public class ModalidadesDaoImpl 
	extends GenericDaoImpl<ModalidadesEntity, Long> 
	implements IModalidadesDao 
{

}
