package es.springsuite.mvc.dao.common.impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.common.IEstadosDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.common.EstadosEntity;

@Repository
public class EstadosDaoImpl 
	extends GenericDaoImpl<EstadosEntity, Long> 
	implements IEstadosDao {

}
