package es.springsuite.mvc.dao.common;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.common.GruposCategoriasEntity;

public interface IGruposCategoriasDao extends IGenericDao<GruposCategoriasEntity, Long> {

}
