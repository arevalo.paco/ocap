package es.springsuite.mvc.dao.common;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.common.EstadosEntity;

public interface IEstadosDao extends IGenericDao<EstadosEntity, Long> {

}
