package es.springsuite.mvc.dao.common.impl;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.common.IProvinciasDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.common.ProvinciasEntity;

@Repository
public class ProvinciasDaoImpl 
  extends  GenericDaoImpl<ProvinciasEntity, String> 
  implements IProvinciasDao {

}
