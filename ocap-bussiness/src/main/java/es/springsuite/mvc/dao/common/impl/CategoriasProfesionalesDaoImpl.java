package es.springsuite.mvc.dao.common.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.common.ICategoriasProfesionalesDao;
import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.entities.common.CategoriasProfesionalesEntity;

@Repository
public class CategoriasProfesionalesDaoImpl 
	extends GenericDaoImpl<CategoriasProfesionalesEntity, Long> 
	implements ICategoriasProfesionalesDao 
{

	
	@Override
	public List<?> listadoPrueba() {
		try  {
			
		   Session session = getSession();
		   CriteriaBuilder cb = session.getCriteriaBuilder();
		    CriteriaQuery<CategoriasProfesionalesEntity> cq = cb.createQuery(CategoriasProfesionalesEntity.class);
		    Root<CategoriasProfesionalesEntity> rootEntry = cq.from(CategoriasProfesionalesEntity.class);
		    CriteriaQuery<CategoriasProfesionalesEntity> all = cq.select(rootEntry);
		 
		    TypedQuery<CategoriasProfesionalesEntity> allQuery = session.createQuery(all);
		    return allQuery.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
