package es.springsuite.mvc.dao.usuarios.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import es.springsuite.mvc.dao.impl.GenericDaoImpl;
import es.springsuite.mvc.dao.usuarios.IUsuariosDao;
import es.springsuite.mvc.entities.usuarios.UsuariosEntity;
import groovy.util.logging.Slf4j;

@Slf4j
@Repository
public class UsuariosDaoImpl extends GenericDaoImpl<UsuariosEntity, Long> implements IUsuariosDao {

	@Override
	public UsuariosEntity checkUsuario(String usuario, String password) {

		try {
			CriteriaBuilder builder = getSession().getCriteriaBuilder();
			CriteriaQuery<UsuariosEntity> criteriaQuery = builder.createQuery(UsuariosEntity.class);
			Root<UsuariosEntity> root = criteriaQuery.from(UsuariosEntity.class);

			List<Predicate> filters = new ArrayList<Predicate>();

			filters.add(builder.equal(root.get("dLogin"), usuario));
			filters.add(builder.equal(root.get("password"), password));

			Predicate And = builder.and(filters.toArray(new Predicate[filters.size()]));

			// where
			criteriaQuery.where(And);

			Query query = getSession().createQuery(criteriaQuery);

			return (UsuariosEntity) query.getResultList().get(0);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
}
