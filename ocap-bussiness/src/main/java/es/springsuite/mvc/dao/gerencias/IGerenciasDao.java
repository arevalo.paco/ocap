package es.springsuite.mvc.dao.gerencias;

import es.springsuite.mvc.dao.IGenericDao;
import es.springsuite.mvc.entities.gerencias.GerenciasEntity;

public interface IGerenciasDao extends IGenericDao<GerenciasEntity, Long> {

}
