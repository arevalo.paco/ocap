package es.springsuite.mvc.services.common;

import java.util.List;

import es.springsuite.mvc.entities.gerencias.GerenciasEntity;

public interface IGerenciasService {

    List<GerenciasEntity> listarGerencias();
    
    GerenciasEntity getGerenciaById(Long id);
    
    void guardarGerencia(GerenciasEntity gerenciaEntity);
    
    void eliminarGerencia(Long id);
}
