/**
 * 
 */
package es.springsuite.mvc.services.usuarios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.springsuite.mvc.dao.usuarios.IUsuariosDao;
import es.springsuite.mvc.entities.usuarios.UsuariosEntity;
import es.springsuite.mvc.services.usuarios.IUsuariosService;

/**
 * 
 * Servicio de administración de usuarios y login
 * 
 * @author farevalo
 */
@Service
public class UsuariosServiceImpl implements IUsuariosService {

	@Autowired
	private IUsuariosDao usuariosDao;
	
	@Override
	public UsuariosEntity login(String user, String password) {
			
		UsuariosEntity usuario = usuariosDao.checkUsuario(user, password);

		return usuario;
	}

}
