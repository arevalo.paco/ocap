package es.springsuite.mvc.services.usuarios;

import es.springsuite.mvc.entities.usuarios.UsuariosEntity;

public interface IUsuariosService {

	/**
	 * Función que comprueba que el usuario existe 
	 * 
	 * @param user
	 * @param password
	 * @return UsuariosEntity 
	 */
	public UsuariosEntity login(String user, String password);
}
