package es.springsuite.mvc.services.common.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.springsuite.mvc.dao.gerencias.IGerenciasDao;
import es.springsuite.mvc.entities.gerencias.GerenciasEntity;
import es.springsuite.mvc.services.common.IGerenciasService;

@Service
public class GerenciasServiceImpl implements IGerenciasService {

    @Autowired
    private IGerenciasDao gerenciasDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GerenciasEntity> listarGerencias() {
        return gerenciasDao.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GerenciasEntity getGerenciaById(Long id) {
        return gerenciasDao.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void guardarGerencia(GerenciasEntity gerenciaEntity) {
        if (gerenciaEntity.getcGerenciaId() == null) {
            gerenciasDao.insert(gerenciaEntity);
        } else {
            gerenciasDao.patch(gerenciaEntity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void eliminarGerencia(Long id) {
        gerenciasDao.delete(id);
    }

}
