package es.springsuite.mvc.services.common;

import java.util.List;

import es.springsuite.mvc.entities.common.CategoriasProfesionalesEntity;
import es.springsuite.mvc.entities.common.EspecialidadesEntity;
import es.springsuite.mvc.entities.common.EstadosEntity;
import es.springsuite.mvc.entities.common.GruposCategoriasEntity;
import es.springsuite.mvc.entities.common.ModalidadesEntity;
import es.springsuite.mvc.entities.common.ProvinciasEntity;
import es.springsuite.mvc.entities.estatutos.EstatutarioEntity;
import es.springsuite.mvc.entities.gerencias.GerenciasEntity;

public interface ITablasComunesService {
	
	List<EstadosEntity> getListaEstados();

	List<CategoriasProfesionalesEntity> getListaCategoriasProfesionales();

	CategoriasProfesionalesEntity getCategoriasProfesionalesById(Long id);

    CategoriasProfesionalesEntity guardarCategProfesional(CategoriasProfesionalesEntity entity);

    List<EspecialidadesEntity> getListaEspecialidades();
    
    EspecialidadesEntity getEspecialidadById(Long id);
    
    EspecialidadesEntity guardarEspecialidad(EspecialidadesEntity entity);

    List<GerenciasEntity> getListaGerencias();

    GerenciasEntity getGerenciaById(Long id);

    GerenciasEntity guardarGerencia(GerenciasEntity entity);

    List<ProvinciasEntity> getListaProvincias();
    
    List<EstatutarioEntity> getListaEstatutos();

    List<ModalidadesEntity> getListaModalidades();
    
    List<GruposCategoriasEntity> getGruposCategorias();
}
