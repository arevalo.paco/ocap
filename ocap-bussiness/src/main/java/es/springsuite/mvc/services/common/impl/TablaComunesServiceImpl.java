package es.springsuite.mvc.services.common.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.springsuite.mvc.dao.common.ICategoriasProfesionalesDao;
import es.springsuite.mvc.dao.common.IEspecialidadesDao;
import es.springsuite.mvc.dao.common.IEstadosDao;
import es.springsuite.mvc.dao.common.IGruposCategoriasDao;
import es.springsuite.mvc.dao.common.IModalidadesDao;
import es.springsuite.mvc.dao.common.IProvinciasDao;
import es.springsuite.mvc.dao.estatutos.IEstatutarioDao;
import es.springsuite.mvc.dao.gerencias.IGerenciasDao;
import es.springsuite.mvc.entities.common.CategoriasProfesionalesEntity;
import es.springsuite.mvc.entities.common.EspecialidadesEntity;
import es.springsuite.mvc.entities.common.EstadosEntity;
import es.springsuite.mvc.entities.common.GruposCategoriasEntity;
import es.springsuite.mvc.entities.common.ModalidadesEntity;
import es.springsuite.mvc.entities.common.ProvinciasEntity;
import es.springsuite.mvc.entities.estatutos.EstatutarioEntity;
import es.springsuite.mvc.entities.gerencias.GerenciasEntity;
import es.springsuite.mvc.services.common.ITablasComunesService;

/**
 * Servicio que da acceso a las tablas Auxiliares
 * 
 * @author farevalo
 */
@Service
public class TablaComunesServiceImpl implements ITablasComunesService {

    @Autowired
    private IEstadosDao estadosDao;

    @Autowired
    private ICategoriasProfesionalesDao categoriasProfesionalesDao;

    @Autowired
    private IEspecialidadesDao especialidadesDao;
    
    @Autowired
    private IGerenciasDao gerenciasDao;
    
    @Autowired
    private IProvinciasDao provinciasDao;
    
    @Autowired
    private IEstatutarioDao estaturarioDao;
    
    @Autowired
    private IModalidadesDao modalidadesDao;

    @Autowired
    private IGruposCategoriasDao gruposCategoriasDao;
    
    @Override
    public List<EstadosEntity> getListaEstados() {
        return estadosDao.findAll();
    }

    @Override
    public List<CategoriasProfesionalesEntity> getListaCategoriasProfesionales() {
        return categoriasProfesionalesDao.findAll();
    }

    @Override
    public CategoriasProfesionalesEntity guardarCategProfesional(CategoriasProfesionalesEntity entity) {
        if (entity.getcProfesionalId() == null) {
            return categoriasProfesionalesDao.insert(entity);
        } else {
            return categoriasProfesionalesDao.patch(entity);
        }

    }
    
    @Override
    public CategoriasProfesionalesEntity getCategoriasProfesionalesById(Long id) {
        return categoriasProfesionalesDao.findById(id);
    }

    //-------------------------------------------------------------
    @Override
    public List<EspecialidadesEntity> getListaEspecialidades() {
        return especialidadesDao.findAll();
    }

    @Override
    public EspecialidadesEntity getEspecialidadById(Long id) {
        return especialidadesDao.findById(id);
    }

    @Override
    public EspecialidadesEntity guardarEspecialidad(EspecialidadesEntity entity) {
        if (entity.getcEspecId() == null) {
            return especialidadesDao.insert(entity);
        } else {
            return especialidadesDao.patch(entity);
        }
    }

    //-----------------------------------------------------------
    @Override
    public List<GerenciasEntity> getListaGerencias() {
        return gerenciasDao.findAll();
    }

    @Override
    public GerenciasEntity getGerenciaById(Long id) {
        return gerenciasDao.findById(id);
    }
    
    @Override
    public GerenciasEntity guardarGerencia(GerenciasEntity entity) {
        if (entity.getcGerenciaId()==null) {
            return gerenciasDao.insert(entity);
        } else {
            return gerenciasDao.patch(entity);
        }
    }

    //----------------------------------------------------------- 
    @Override
    public List<ProvinciasEntity> getListaProvincias() {
        return provinciasDao.findAll();
    }

    //----------------------------------------------------------- 
    @Override
    public List<EstatutarioEntity> getListaEstatutos() {
        return estaturarioDao.findAll();
    }
    
    //-----------------------------------------------------------
    @Override
    public List<ModalidadesEntity> getListaModalidades() {
        return modalidadesDao.findAll();
    }
    
    //-----------------------------------------------------------
    @Override
    public List<GruposCategoriasEntity> getGruposCategorias() {
        return gruposCategoriasDao.findAll();
    }
}