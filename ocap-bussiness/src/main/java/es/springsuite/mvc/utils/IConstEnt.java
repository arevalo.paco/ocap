package es.springsuite.mvc.utils;

public interface IConstEnt extends IFields {

    short LENGHT_1 = 1;
	short LENGHT_2 = 2;
    short LENGHT_6 = 6;
    short LENGHT_11 = 11;
	short LENGHT_15 = 15;
	short LENGHT_20 = 20;
	short LENGHT_30 = 30;
	short LENGHT_40 = 40;
	short LENGHT_50 = 50;
	short LENGHT_60 = 60;
	short LENGHT_100 = 100;
	short LENGHT_200 = 200;
	short LENGHT_255 = 255;
	
	// Entidades
	String MODALIDAD_ENTITY = "modalidadesEntity";
	String GRUPO_CATEGORIAS_ENTITY = "gruposCategoriasEntity";
	String TIPO_GERENCIAS_ENTITY = "tipoGerenciasEntity";
	String PROVINCIAS_ENTITY = "provinciaEntity";
    
	String PUNTO = ".";
}
