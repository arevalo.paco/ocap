package es.springsuite.mvc.utils;

public interface IFields {

    
     String C_PROFESIONAL_ID = "cProfesionalId";
     String CESTATUDID = "cEstatutId";
     String D_NOMBRE = "dNombre";
     String CUSUALTA = "cUsuAlta";

     String FUSUALTA = "fUsuAlta";
     String BBORRADO = "bBorrado";
     String CUSUMODI = "cUsuModi";
     String FUSUMODI = "fUsuModi";

     String DDESCRIPCION = "dDescripcion";

     String C_GRUPOCATEGORIA_ID = "cGrupoCategoriaId";
     String C_ESTADO_ID = "cEstadoId";
     String C_FASE = "cFase";

     String C_PROVINCIA_ID = "cProvinciaId";
     String D_PROVINCIA = "dProvincia";
     String C_MODALIDAD_ID = "cModalidadId";
     
     String C_GERENCIA_ID = "cGerenciaId";
     String D_NOMBRE_CORTO = "dNombreCorto";
     String A_CODLDAP = "aCodLdap";
     String D_GERENTE = "dGerente";
     String D_DIRECTOR = "dDirector";
}
