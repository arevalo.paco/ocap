package es.springsuite.mvc.entities.usuarios;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "OCAP_USUARIOS")
@Data
@SuperBuilder
public class UsuariosEntity implements Serializable {

	private static final long serialVersionUID = 3435799953949126110L;

	public UsuariosEntity() {}
	
	@Id
	@Column(name = "C_USR_ID")
	private String c_usr_id;

	@Column(name = "d_Login" , length = IConstEnt.LENGHT_20)
	private String dLogin;

	@Column(name = "PASSWORD" , length = IConstEnt.LENGHT_20)
	private String password;

	@Column(name = "C_CENTROTRABAJO_ID", nullable = false)
	private Integer cCentroTrabajoId;
	
	@Column(name = "C_PROFESIONAL_ID", nullable = false)
	private Integer cProfesionalId;
	
	@Column(name = "C_GERENCIA_ID", nullable = false)
	private Integer cGerenciaId;

	@Column(name = "D_APELLIDOS" , length = IConstEnt.LENGHT_60, nullable = false)
	private String dApellidos;

	@Column(name = "D_NOMBRE" , length = IConstEnt.LENGHT_60, nullable = false)
	private String dNombre;

	@Column(name = "B_SEXO", length = IConstEnt.LENGHT_1, nullable = false)
	private String bSexo;

	@Column(name = "F_USUALTA")
	private Date fUsuAlta;

	@Column(name = "C_ESPEC_ID")
	private Integer cEspecId;

	@Column(name = "C_PERFIL_ID")
	private Integer cPerfilId;

	@Column(name = "C_DNI" , length = IConstEnt.LENGHT_11)
	private String cDni;
	
	@Column(name = "A_CORREOELECTRONICO" , length = IConstEnt.LENGHT_50)
	private String aCorreoElectronico;
	
	@Column(name = "A_DOMICILIO" , length = IConstEnt.LENGHT_100)
	private String aDomicilio;

	@Column(name = "C_PROVINCIA_ID" , length = IConstEnt.LENGHT_2)
	private String cProvinciaId;

	@Column(name = "D_LOCAL" , length = IConstEnt.LENGHT_40)
	private String dLocal;

	@Column(name = "N_CODIGOPOSTAL", precision = 5, scale = 1)
	private Integer nCodigoPostal;

	@Column(name = "N_TELEFONO1", precision = 9, scale = 0)
	private Integer nTelefono1;

	@Column(name = "N_TELEFONO2", precision = 9, scale = 0)
	private Integer nTelefono2;
	
	@Column(name = "C_DNI_REAL" , length = IConstEnt.LENGHT_11)
	private String cDniReal;
	
	@Column(name = "B_BORRADO", length = IConstEnt.LENGHT_1)
	private String bBorrado;
	
	@Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
	private String cUsuModi;
	
	@Column(name = "F_UsuModi")
	private Date fUsuModi;

}