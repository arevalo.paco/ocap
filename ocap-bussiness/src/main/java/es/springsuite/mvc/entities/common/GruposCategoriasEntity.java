package es.springsuite.mvc.entities.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * OCAP_GRUPO_CATEGORIAS: Grupos de Categorias profesionales
 * 
 * @author farevalg
 */
@Entity
@Table(name = "OCAP_GRUPO_CATEGORIAS")
@Data
public class GruposCategoriasEntity implements Serializable {    

    private static final long serialVersionUID = 1919487928732186391L;

    @Id
    @Column(name = "C_GRUPO_CATEGORIA_ID")
    private Long cGrupoCategoriaId;
    
    @Column(name = "C_CODIGO", length = IConstEnt.LENGHT_60,nullable = false)
    private String cCodigo;

    @Column(name = "d_nombre", length = IConstEnt.LENGHT_60, nullable = false)
    private String dNombre;

    @Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30, nullable = false)
    private String cUsuAlta;
    
    @Column(name = "F_USUALTA", nullable = false)
    private Date fUsuAlta = new Date();
    
    @Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
    private String cUsuModi;
    
    @Column(name = "F_USUMODI")
    private Date fUsuModi;

    @Column(name = "b_Borrado", length = IConstEnt.LENGHT_1)
    private String bBorrado;

    public Long getcGrupoCategoriaId() { return cGrupoCategoriaId;    }
    public String getcCodigo()         { return cCodigo;  }
    public String getdNombre()         { return dNombre;   }
    public String getcUsuAlta()        { return cUsuAlta; }
    public Date getfUsuAlta()          { return fUsuAlta; }
    public String getcUsuModi()        { return cUsuModi;  }
    public Date getfUsuModi()          { return fUsuModi; }
    public String getbBorrado()        { return bBorrado; }
    
    public void setcGrupoCategoriaId(Long cGrupoCategoriaId) {    this.cGrupoCategoriaId = cGrupoCategoriaId; }
    public void setcCodigo(String cCodigo)   { this.cCodigo = cCodigo;}
    public void setdNombre(String dNombre)   { this.dNombre = dNombre; }
    public void setcUsuAlta(String cUsuAlta) { this.cUsuAlta = cUsuAlta; }
    public void setfUsuAlta(Date fUsuAlta)   { this.fUsuAlta = fUsuAlta; }
    public void setcUsuModi(String cUsuModi) { this.cUsuModi = cUsuModi; }
    public void setfUsuModi(Date fUsuModi)   { this.fUsuModi = fUsuModi; }
    public void setbBorrado(String bBorrado) { this.bBorrado = bBorrado; }
}
