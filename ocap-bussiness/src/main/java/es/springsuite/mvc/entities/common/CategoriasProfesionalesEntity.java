package es.springsuite.mvc.entities.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import es.springsuite.mvc.entities.estatutos.EstatutarioEntity;
import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * OCAP_CATEG_PROFESIONALES: Categorias  profesionales
 * @author farevalg
 *
 */
@Entity
@Table(name = "OCAP_CATEG_PROFESIONALES")
@Data
public class CategoriasProfesionalesEntity implements Serializable {

	private static final long serialVersionUID = -4928932570730871471L;

	@Id
	@Column(name = "C_PROFESIONAL_ID")
	private Long cProfesionalId;
	
	@Column(name = "C_ESTATUT_ID", nullable = false)
	private Long cEstatutId;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "C_ESTATUT_ID", insertable = false, updatable = false)
	@Fetch(FetchMode.JOIN)
	private EstatutarioEntity estatutarioEntity;
	
	@Column(name = "d_nombre", length = IConstEnt.LENGHT_200, nullable = false)
	private String dNombre;
	

	@Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30, nullable = false)
	private String cUsuAlta;
	
	@Column(name = "F_USUALTA", nullable = false)
	private Date fUsuAlta = new Date();

	@Column(name = "b_Borrado", length = IConstEnt.LENGHT_1)
	private String bBorrado;
	
	@Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
	private String cUsuModi;
	
	@Column(name = "F_USUMODI")
	private Date fUsuModi;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "c_modalidad_id", insertable = false, updatable = false)
	@Fetch(FetchMode.JOIN)
	private ModalidadesEntity modalidadesEntity;
	
	@Column(name = "c_modalidad_id")
	private Long cModalidadId; 
	
	@Column(name = "d_descripcion", length = IConstEnt.LENGHT_200)
	private String dDescripcion;

	@Column(name = "c_grupo_categoria_id")
	private Integer cGrupoCategoriaId;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "c_grupo_categoria_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private GruposCategoriasEntity gruposCategoriasEntity;
    
	public String getcProfesionalId() 	        { return (cProfesionalId == null ? "-1" : cProfesionalId.toString()); }
	public Long getcEstatutId() 		        { return cEstatutId; }
	public String getdNombre() 			        { return dNombre;	}
	public String getcUsuAlta() 		        { return cUsuAlta; }
	public Date getfUsuAlta() 			        { return fUsuAlta; }
	public String getbBorrado() 		        { return bBorrado;}
	public String getcUsuModi() 		        { return cUsuModi; }
	public Date getfUsuModi() 			        { return fUsuModi; }
	public Long getcModalidadId() 	            { return cModalidadId;	}
	public ModalidadesEntity getModalidadesEntity()    { return modalidadesEntity; }
	public String getdDescripcion()		        { return dDescripcion;	}
	public Integer getcGrupoCategoriaId()       { return cGrupoCategoriaId; }
	public GruposCategoriasEntity getGruposCategoriasEntity() { return gruposCategoriasEntity; }

	public void setcProfesionalId(Long cProfesionalId )     { this.cProfesionalId = cProfesionalId;}
	public void setcEstatutId(Long cEstatutId)              { this.cEstatutId = cEstatutId;}
	public void setdNombre(String dNombre)                  { this.dNombre = dNombre; }
	public void setcUsuAlta(String cUsuAlta)                { this.cUsuAlta = cUsuAlta; }
	public void setfUsuAlta(Date fUsuAlta)                  { this.fUsuAlta = fUsuAlta; }
	public void setbBorrado(String bBorrado)                { this.bBorrado = bBorrado; }
	public void setcUsuModi(String cUsuModi)                { this.cUsuModi = cUsuModi;}
	public void setfUsuModi(Date fUsuModi)                  { this.fUsuModi = fUsuModi; }
	public void setcModalidadId(Long cModalidadId)          { this.cModalidadId = cModalidadId; }
	public void setModalidadesEntity(ModalidadesEntity modalidadesEntity) { this.modalidadesEntity = modalidadesEntity; }
	public void setdDescripcion(String dDescripcion)        { this.dDescripcion = dDescripcion; }
	public void setcGrupoCategoriaId(Integer cGrupoCategoriaId) { this.cGrupoCategoriaId = cGrupoCategoriaId;}
	public void setGruposCategoriasEntity(GruposCategoriasEntity gruposCategoriasEntity) { this.gruposCategoriasEntity = gruposCategoriasEntity; }

}
