package es.springsuite.mvc.entities.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * COMU_PROVINCIAS: Provincias (Esquema COMU)
 * 
 * @author farevalo
 *
 */
@Entity
@Table(name = "COMU_PROVINCIAS")
@Data
public class ProvinciasEntity implements Serializable {

    private static final long serialVersionUID = -2776553741931704248L;

    @Id
    @Column(name = "C_PROVINCIA_ID", length = IConstEnt.LENGHT_2)
    private String cProvinciaId;
    
    @Column(name = "D_PROVINCIA", length = IConstEnt.LENGHT_50, nullable = false)
    private String dProvincia;
    
    @Column(name = "C_CCAA", length = IConstEnt.LENGHT_2)
    private String cCcaa;
    
    @Column(name = "D_CCAA", length = IConstEnt.LENGHT_50)
    private String dCcaa;

    public String getcProvinciaId() { return cProvinciaId; }
    public String getdProvincia()   { return dProvincia; }
    public String getcCcaa()        { return cCcaa; }
    public String getdCcaa()        { return dCcaa; }

    public void setcProvinciaId(String cProvinciaId) { this.cProvinciaId = cProvinciaId; }
    public void setdProvincia(String dProvincia)     { this.dProvincia = dProvincia; }
    public void setcCcaa(String cCcaa)               { this.cCcaa = cCcaa; }
    public void setdCcaa(String dCcaa)               { this.dCcaa = dCcaa; }
    
}
