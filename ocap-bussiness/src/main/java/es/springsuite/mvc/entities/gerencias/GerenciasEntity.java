package es.springsuite.mvc.entities.gerencias;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.springsuite.mvc.entities.common.ProvinciasEntity;
import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * OCAP_GERENCIAS: Gerencias en Castilla y León
 * 
 * @author farevalo
 *
 */
@Entity
@Table(name = "OCAP_GERENCIAS")
@Data
public class GerenciasEntity implements Serializable {

    private static final long serialVersionUID = 745274377499196239L;

    @Id
    @Column(name = "C_GERENCIA_ID")
    private Long cGerenciaId;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "C_PROVINCIA_ID", nullable = false)
    private ProvinciasEntity provinciaEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "C_TIPOGERENCIA_ID", nullable = false)
    private TipoGerenciasEntity tipoGerenciasEntity;

    @Column(name = "D_NOMBRE_CORTO", length = IConstEnt.LENGHT_100, nullable = false)
    private String dNombreCorto;
    
    @Column(name = "D_NOMBRE", length = IConstEnt.LENGHT_200, nullable = false)
    private String dNombre;
    
    @Column(name = "A_CODLDAP", length = IConstEnt.LENGHT_6, nullable = false)
    private String aCodldap;
    
    @Column(name = "F_USUALTA", nullable = false)
    private Date fUsuAlta;
    
    @Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30, nullable = false)
    private String cUsuAlta;
    
    @Column(name = "B_BORRADO", length = IConstEnt.LENGHT_1, nullable = false)
    private String bBorrado;

    @Column(name = "D_GERENTE", length = IConstEnt.LENGHT_100)
    private String dGerente;

    @Column(name = "D_DIRECTOR", length = IConstEnt.LENGHT_100)
    private String dDirector;
    
    @Column(name = "F_USUMODI")
    private Date fUsuModi;
    
    @Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
    private String cUsuModi;

    public Long getcGerenciaId()        { return cGerenciaId; }
    public String getdNombreCorto()     { return dNombreCorto; }
    public String getdNombre()          { return dNombre; }
    public String getaCodldap()         { return aCodldap; }
    public Date getfUsuAlta()           { return fUsuAlta; }
    public String getcUsuAlta()         { return cUsuAlta; }
    public String getbBorrado()         { return bBorrado; }
    public String getdGerente()         { return dGerente; }
    public String getdDirector()        { return dDirector; }
    public Date getfUsuModi()           { return fUsuModi; }
    public String getcUsuModi()         { return cUsuModi; }
    public ProvinciasEntity getProvinciaEntity()       { return provinciaEntity; }
    public TipoGerenciasEntity getTipoGerenciasEntity() { return tipoGerenciasEntity; }

    public void setcGerenciaId(Long cGerenciaId)     { this.cGerenciaId = cGerenciaId; }
    public void setdNombreCorto(String dNombreCorto) { this.dNombreCorto = dNombreCorto; }
    public void setdNombre(String dNombre)           { this.dNombre = dNombre; }
    public void setaCodldap(String aCodldap)         { this.aCodldap = aCodldap; }
    public void setfUsuAlta(Date fUsuAlta)           { this.fUsuAlta = fUsuAlta; }
    public void setcUsuAlta(String cUsuAlta)         { this.cUsuAlta = cUsuAlta; }
    public void setbBorrado(String bBorrado)         { this.bBorrado = bBorrado; }
    public void setdGerente(String dGerente)         { this.dGerente = dGerente; }
    public void setdDirector(String dDirector)       { this.dDirector = dDirector; }
    public void setfUsuModi(Date fUsuModi)           { this.fUsuModi = fUsuModi; }
    public void setcUsuModi(String cUsuModi)         { this.cUsuModi = cUsuModi; }
    public void setProvinciaEntity(ProvinciasEntity provinciaEntity)            { this.provinciaEntity = provinciaEntity; }
    public void setTipoGerenciasEntity(TipoGerenciasEntity tipoGerenciasEntity) { this.tipoGerenciasEntity = tipoGerenciasEntity; }

}