package es.springsuite.mvc.entities.estatutos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

@Entity
@Table(name = "OCAP_ESTATUTARIO")
@Data
public class EstatutarioEntity implements Serializable {
    
    private static final long serialVersionUID = 3241564603604420557L;

    @Id
    @Column(name = "c_estatut_id", scale = IConstEnt.LENGHT_6, precision = 0, nullable = false)
    private Long cEstatutId;

    @Column(name = "c_personal_id", scale = IConstEnt.LENGHT_6, precision = 0)
    private Long cPersonalId;
    
    @Column(name = "d_nombre", length = IConstEnt.LENGHT_100, nullable = false)
    private String dNombre;

    @Column(name = "d_descripcion", length = IConstEnt.LENGHT_200, nullable = false)
    private String dDescripcion;

    @Column(name = "b_Borrado", length = IConstEnt.LENGHT_1)
    private String bBorrado;
    
    @Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30)
    private String cUsuAlta;
    
    @Column(name = "F_USUALTA")
    private Date fUsuAlta;

    @Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
    private String cUsuModi;
    
    @Column(name = "F_USUMODI")
    private Date fUsuModi;

    public Long getcEstatutId()     { return cEstatutId; }
    public String getcEstatutIdS()  { return cEstatutId.toString(); }
    public Long getcPersonalId()    { return cPersonalId; }
    public String getdNombre()      { return dNombre; }
    public String getdDescripcion() { return dDescripcion; }
    public String getbBorrado()     { return bBorrado; }
    public String getcUsuAlta()     { return cUsuAlta; }
    public Date getfUsuAlta()       { return fUsuAlta; }
    public String getcUsuModi()     { return cUsuModi; }
    public Date getfUsuModi()       { return fUsuModi; }
    
    public void setcEstatutId(Long cEstatutId)        { this.cEstatutId = cEstatutId; }
    public void setcPersonalId(Long cPersonalId)      { this.cPersonalId = cPersonalId; } 
    public void setdNombre(String dNombre)            { this.dNombre = dNombre; }
    public void setdDescripcion(String dDescripcion)  { this.dDescripcion = dDescripcion; }
    public void setbBorrado(String bBorrado)          { this.bBorrado = bBorrado; }
    public void setcUsuAlta(String cUsuAlta)          { this.cUsuAlta = cUsuAlta; }
    public void setfUsuAlta(Date fUsuAlta)            { this.fUsuAlta = fUsuAlta; }
    public void setcUsuModi(String cUsuModi)          { this.cUsuModi = cUsuModi;}
    public void setfUsuModi(Date fUsuModi)            { this.fUsuModi = fUsuModi; }

}