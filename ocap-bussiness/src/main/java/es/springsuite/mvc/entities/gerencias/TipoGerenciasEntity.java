package es.springsuite.mvc.entities.gerencias;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * OCAP_TIPOGERENCIAS: Tipo de Gerencias en Castilla y León
 * 
 * @author farevalo
 *
 */
@Entity
@Table(name = "OCAP_TIPOGERENCIAS")
@Data
public class TipoGerenciasEntity implements Serializable {


    private static final long serialVersionUID = 4638637145289009824L;

    @Id
    @Column(name = "C_TIPOGERENCIA_ID")
    private Long cGerenciaId;

    @Column(name = "D_NOMBRE", length = IConstEnt.LENGHT_100, nullable = false)
    private String dNombre;

    @Column(name = "D_DESCRIPCION", length = IConstEnt.LENGHT_200, nullable = true)
    private String dDescripcion;

    @Column(name = "B_BORRADO", length = IConstEnt.LENGHT_1, nullable = false)
    private String bBorrado;

    @Column(name = "F_USUALTA", nullable = false)
    private Date fUsuAlta;
    
    @Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30, nullable = false)
    private String cUsuAlta;
    
    @Column(name = "F_USUMODI", nullable = true)
    private Date fUsuModi;
    
    @Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30, nullable = true)
    private String cUsuModi;

    public Long getcGerenciaId()    { return cGerenciaId; }
    public String getdNombre()      { return dNombre; }
    public String getdDescripcion() { return dDescripcion; }
    public String getbBorrado()     { return bBorrado; }
    public Date getfUsuAlta()       { return fUsuAlta; }
    public String getcUsuAlta()     { return cUsuAlta; }
    public Date getfUsuModi()       { return fUsuModi; }
    public String getcUsuModi()     { return cUsuModi; }

    public void setcGerenciaId(Long cGerenciaId)        { this.cGerenciaId = cGerenciaId; }
    public void setdNombre(String dNombre)              { this.dNombre = dNombre; }
    public void setdDescripcion(String dDescripcion)    { this.dDescripcion = dDescripcion; }
    public void setbBorrado(String bBorrado)            { this.bBorrado = bBorrado; }
    public void setfUsuAlta(Date fUsuAlta)              { this.fUsuAlta = fUsuAlta; }
    public void setcUsuAlta(String cUsuAlta)            { this.cUsuAlta = cUsuAlta; }
    public void setfUsuModi(Date fUsuModi)              { this.fUsuModi = fUsuModi; }
    public void setcUsuModi(String cUsuModi)            { this.cUsuModi = cUsuModi; }

}
