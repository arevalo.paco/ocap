package es.springsuite.mvc.entities.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * OCAP_MODALIDADES: this entity for table <I>OCAP_MODALIDADES</I> 
 * 
 * @author farevalg
 */
@Entity
@Table(name = "OCAP_MODALIDADES")
@Data
public class ModalidadesEntity implements Serializable {

	private static final long serialVersionUID = -4928932570730871471L;

	@Id
	@Column(name = "C_Modalidad_ID")
	private Long cModalidadId;
	
    @Column(name = "d_nombre", length = IConstEnt.LENGHT_200, nullable = false)
	private String dNombre;
	

	@Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30, nullable = false)
	private String cUsuAlta;
	
	@Column(name = "F_USUALTA", nullable = false)
	private Date fUsuAlta = new Date();

	@Column(name = "b_Borrado", length = IConstEnt.LENGHT_1)
	private String bBorrado;
	
	@Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
	private String cUsuModi;
	
	@Column(name = "F_USUMODI")
	private Date fUsuModi;

	@Column(name = "d_descripcion", length = IConstEnt.LENGHT_200)
	private String dDescripcion;

	

    public Long getcModalidadId()           { return cModalidadId; }
    public String getcModalidadIdToString() { return cModalidadId.toString(); }
    public String getdNombre() 	    		{ return dNombre;	}
	public String getcUsuAlta() 	  	    { return cUsuAlta; }
	public Date getfUsuAlta() 			    { return fUsuAlta; }
	public String getbBorrado()        		{ return bBorrado;}
	public String getcUsuModi()        		{ return cUsuModi; }
	public Date getfUsuModi() 			    { return fUsuModi; }
	public String getdDescripcion()		    { return dDescripcion;	}

	public void setcModalidadId(Long cModalidadId)         { this.cModalidadId = cModalidadId; }
	public void setdNombre(String dNombre)                 { this.dNombre = dNombre; }
	public void setcUsuAlta(String cUsuAlta)               { this.cUsuAlta = cUsuAlta; }
	public void setfUsuAlta(Date fUsuAlta)                 { this.fUsuAlta = fUsuAlta; }
	public void setbBorrado(String bBorrado)               { this.bBorrado = bBorrado; }
	public void setcUsuModi(String cUsuModi)               { this.cUsuModi = cUsuModi;}
	public void setfUsuModi(Date fUsuModi)                 { this.fUsuModi = fUsuModi; }
	public void setdDescripcion(String dDescripcion)       { this.dDescripcion = dDescripcion; }

}
