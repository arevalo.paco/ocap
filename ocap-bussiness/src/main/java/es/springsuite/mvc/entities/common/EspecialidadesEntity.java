package es.springsuite.mvc.entities.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

/**
 * OCAP_ESPECIALIDADES: Especialidades profesionales
 * 
 * @author PACO
 *
 */
@Entity
@Table(name = "OCAP_ESPECIALIDADES")
@Data
public class EspecialidadesEntity implements Serializable {

	private static final long serialVersionUID = -4928932570730871471L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="especIdGenerator")
	@SequenceGenerator(name="especIdGenerator", sequenceName="OCAP_ESP_ID_SEQ")
	@Column(name = "C_ESPEC_ID")
	private Long cEspecId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "C_Profesional_ID")
	@Fetch(FetchMode.JOIN)
	private CategoriasProfesionalesEntity categoriasProfesional;
	
	@Column(name = "d_nombre", length = IConstEnt.LENGHT_100)
	private String dNombre;
	
	@Column(name = "d_descripcion", length = IConstEnt.LENGHT_200)
	private String dDescripcion;
	
	@Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30, nullable = false)
	private String cUsuAlta;
	
	@Column(name = "F_USUALTA", nullable = false)
	private Date fUsuAlta = new Date();
	
	@Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
	private String cUsuModi;
	
	@Column(name = "F_USUMODI")
	private Date fUsuModi;

	@Column(name = "b_Borrado", length = IConstEnt.LENGHT_1)
	private String bBorrado;

    public Long getcEspecId()        { return cEspecId; }
    public String getdNombre()       { return dNombre; }
    public String getdDescripcion()  { return dDescripcion; }
    public String getcUsuAlta()      { return cUsuAlta; }
    public Date getfUsuAlta()        { return fUsuAlta; }
    public String getcUsuModi()      { return cUsuModi; }
    public Date getfUsuModi()        { return fUsuModi; }
    public String getbBorrado()      { return bBorrado;   }
    public CategoriasProfesionalesEntity getCategoriasProfesional() { return categoriasProfesional; }

    public void setcEspecId(Long cEspecId)           { this.cEspecId = cEspecId; }
    public void setdNombre(String dNombre)           { this.dNombre = dNombre;  }
    public void setdDescripcion(String dDescripcion) { this.dDescripcion = dDescripcion; }
    public void setcUsuAlta(String cUsuAlta)         { this.cUsuAlta = cUsuAlta; }
    public void setfUsuAlta(Date fUsuAlta)           { this.fUsuAlta = fUsuAlta; }
    public void setcUsuModi(String cUsuModi)         { this.cUsuModi = cUsuModi; }
    public void setfUsuModi(Date fUsuModi)           { this.fUsuModi = fUsuModi; }
    public void setbBorrado(String bBorrado)         { this.bBorrado = bBorrado; }
    public void setCategoriasProfesional(CategoriasProfesionalesEntity categoriasProfesional) { this.categoriasProfesional = categoriasProfesional; }

}
