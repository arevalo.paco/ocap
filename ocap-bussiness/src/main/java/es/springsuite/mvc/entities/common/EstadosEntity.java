package es.springsuite.mvc.entities.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.springsuite.mvc.utils.IConstEnt;
import lombok.Data;

@Entity
@Table(name = "OCAP_ESTADOS")
@Data
public class EstadosEntity implements Serializable {

	private static final long serialVersionUID = -4928932570730871471L;

	@Id
	@Column(name = "c_estado_id")
	private Long cEstadoId;

	@Column(name = "d_nombre", length = IConstEnt.LENGHT_50, nullable = false)
	private String dNombre;
	
	@Column(name = "b_Listado", length = IConstEnt.LENGHT_1)
	private String bListado;

	@Column(name = "b_Borrado", length = IConstEnt.LENGHT_1)
	private String bBorrado;
	
	@Column(name = "C_USUALTA", length = IConstEnt.LENGHT_30)
	private String cUsuAlta;
	
	@Column(name = "F_USUALTA")
	private Date fUsuAlta;
	
	@Column(name = "d_descripcion", length = IConstEnt.LENGHT_60, nullable = false)
	private String dDescripcion;
	
	@Column(name = "c_Fase", length = IConstEnt.LENGHT_1)
	private String cFase;
	
	@Column(name = "C_USUMODI", length = IConstEnt.LENGHT_30)
	private String cUsuModi;
	
	@Column(name = "F_USUMODI")
	private Date fUsuModi;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Long getcEstadoId() {
        return cEstadoId;
    }

    public String getdNombre() {
        return dNombre;
    }

    public String getbListado() {
        return bListado;
    }

    public String getbBorrado() {
        return bBorrado;
    }

    public String getcUsuAlta() {
        return cUsuAlta;
    }

    public Date getfUsuAlta() {
        return fUsuAlta;
    }

    public String getdDescripcion() {
        return dDescripcion;
    }

    public String getcFase() {
        return cFase;
    }

    public String getcUsuModi() {
        return cUsuModi;
    }

    public Date getfUsuModi() {
        return fUsuModi;
    }

    public void setcEstadoId(Long cEstadoId) {
        this.cEstadoId = cEstadoId;
    }

    public void setdNombre(String dNombre) {
        this.dNombre = dNombre;
    }

    public void setbListado(String bListado) {
        this.bListado = bListado;
    }

    public void setbBorrado(String bBorrado) {
        this.bBorrado = bBorrado;
    }

    public void setcUsuAlta(String cUsuAlta) {
        this.cUsuAlta = cUsuAlta;
    }

    public void setfUsuAlta(Date fUsuAlta) {
        this.fUsuAlta = fUsuAlta;
    }

    public void setdDescripcion(String dDescripcion) {
        this.dDescripcion = dDescripcion;
    }

    public void setcFase(String cFase) {
        this.cFase = cFase;
    }

    public void setcUsuModi(String cUsuModi) {
        this.cUsuModi = cUsuModi;
    }

    public void setfUsuModi(Date fUsuModi) {
        this.fUsuModi = fUsuModi;
    }


}
