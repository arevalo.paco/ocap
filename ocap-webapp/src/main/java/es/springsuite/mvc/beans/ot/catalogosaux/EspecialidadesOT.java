package es.springsuite.mvc.beans.ot.catalogosaux;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class EspecialidadesOT implements Serializable {

    private static final long serialVersionUID = -7947072383023182195L;

    @Getter @Setter
	private Long cEspecId;
	
	@Getter @Setter
	private Long categoriaProfesionalId;

	@Getter @Setter
	private String dNombre;

	@Getter @Setter
	private String dDescripcion;
	
	@Getter @Setter
	private String cUsuAlta;
	
	@Getter @Setter
	private Date fUsuAlta = new Date();
	
    @Getter @Setter
	private String cUsuModi;
	
    @Getter @Setter
    private Date fUsuModi;

    @Getter @Setter
	private String bBorrado;

}
