package es.springsuite.mvc.beans.ot.catalogosaux;

import java.util.Date;

import es.springsuite.mvc.entities.common.GruposCategoriasEntity;
import es.springsuite.mvc.entities.common.ModalidadesEntity;

public class CategoriaProfesionalOT {

    private String cProfesionalId;
    
    private Long cEstatutId;

    private String dNombre;
    
    private String cUsuAlta;
    
    private Date fUsuAlta = new Date();

    private String bBorrado;
    
    private String cUsuModi;
    
    private Date fUsuModi;
    
    private Long cModalidadId;
    
    private String dDescripcion;

    private Integer cGrupoCategoriaId;

    private ModalidadesEntity modalidadesEntity;
    
    private GruposCategoriasEntity gruposCategoriasEntity;
    
    
    public String getcProfesionalId()           { return (cProfesionalId == null ? "-1" : cProfesionalId.toString()); }
    public Long getcEstatutId()                 { return cEstatutId; }
    public String getdNombre()                  { return dNombre;   }
    public String getcUsuAlta()                 { return cUsuAlta; }
    public Date getfUsuAlta()                   { return fUsuAlta; }
    public String getbBorrado()                 { return bBorrado;}
    public String getcUsuModi()                 { return cUsuModi; }
    public Date getfUsuModi()                   { return fUsuModi; }
    public Long getcModalidadId()               { return cModalidadId;  }
    public String getdDescripcion()             { return dDescripcion;  }
    public Integer getcGrupoCategoriaId()       { return cGrupoCategoriaId; }
    public ModalidadesEntity getModalidadesEntity()    { return modalidadesEntity; }
    public GruposCategoriasEntity getGruposCategoriasEntity() { return gruposCategoriasEntity; }

    public void setcProfesionalId(String cProfesionalId )     { this.cProfesionalId = cProfesionalId;}
    public void setcEstatutId(Long cEstatutId)                { this.cEstatutId = cEstatutId;}
    public void setdNombre(String dNombre)                    { this.dNombre = dNombre; }
    public void setcUsuAlta(String cUsuAlta)                  { this.cUsuAlta = cUsuAlta; }
    public void setfUsuAlta(Date fUsuAlta)                    { this.fUsuAlta = fUsuAlta; }
    public void setbBorrado(String bBorrado)                  { this.bBorrado = bBorrado; }
    public void setcUsuModi(String cUsuModi)                  { this.cUsuModi = cUsuModi;}
    public void setfUsuModi(Date fUsuModi)                    { this.fUsuModi = fUsuModi; }
    public void setcModalidadId(Long cModalidadId)            { this.cModalidadId = cModalidadId; }
    public void setdDescripcion(String dDescripcion)          { this.dDescripcion = dDescripcion; }
    public void setcGrupoCategoriaId(Integer cGrupoCategoriaId) { this.cGrupoCategoriaId = cGrupoCategoriaId;}
    public void setModalidadesEntity(ModalidadesEntity modalidadesEntity) { this.modalidadesEntity = modalidadesEntity; }
    public void setGruposCategoriasEntity(GruposCategoriasEntity gruposCategoriasEntity) { this.gruposCategoriasEntity = gruposCategoriasEntity; }

}
