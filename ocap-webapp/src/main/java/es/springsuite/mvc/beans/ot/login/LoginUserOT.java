package es.springsuite.mvc.beans.ot.login;

import lombok.Getter;
import lombok.Setter;

public class LoginUserOT {

	@Getter @Setter
	public String username;
	
	@Getter @Setter
	public String password;
}
