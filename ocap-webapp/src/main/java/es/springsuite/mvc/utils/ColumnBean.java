package es.springsuite.mvc.utils;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
//@AllArgsConstructor
public class ColumnBean {

    public interface align {
        String IZDA = "left";
        String DRCHA = "right";
        String CENTRAR = "center";
    }
    

    private String campo;
    private String nombreColumna;
    private int ancho;
    private Boolean editable;
    private Boolean ordenable;
    private Boolean filtrable;
    private String alineacion;
    

    public ColumnBean(String campo, String nombre, int ancho, Boolean editable, Boolean ordenable, Boolean filtrable, String alineacion) {
        this.campo = campo;
        this.nombreColumna = nombre;
        this.ancho = ancho;
        this.editable = editable;
        this.ordenable = ordenable;
        this.filtrable = filtrable;
        this.alineacion = alineacion;
    }
    
    public String toJSONString() {
        return "{ name: \"" +  nombreColumna + "\", width: " + ancho + ", sortable: " + ordenable + 
                ", editable: " + editable + ", search: " + filtrable + ", align: '" + alineacion +"'}";                
    }

    public String getNombre() {
        return nombreColumna;
    }

    public int getAncho() {
        return ancho;
    }

    public Boolean getEditable() {
        return editable;
    }

    public Boolean getOrdenable() {
        return ordenable;
    }

    public Boolean getFiltrable() {
        return filtrable;
    }

    public String getAlineacion() {
        return alineacion;
    }

    public void setNombre(String nombre) {
        this.nombreColumna = nombre;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public void setOrdenable(Boolean ordenable) {
        this.ordenable = ordenable;
    }

    public void setFiltrable(Boolean filtrable) {
        this.filtrable = filtrable;
    }

    public void setAlineacion(String alineacion) {
        this.alineacion = alineacion;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }
}
