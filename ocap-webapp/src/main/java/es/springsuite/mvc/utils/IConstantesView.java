package es.springsuite.mvc.utils;

public interface IConstantesView {
    String ESPECIALIDADES = "especialidadesOT";
    String ESPECIALIDADES_LISTADO = "listadoEspecialidades";

    String ESTATUTOS_LISTADO = "listadoEstatutos";
    String MODALIDADES_LISTADO = "listadoModalidades";
    String GRUPO_CATEGORIAS_LISTADO = "listadoGrupoCategorias";
    
    int LENGTH_1  = 1;
    int LENGTH_5 = 5;
    int LENGTH_10 = 10;
    int LENGTH_20 = 20;
    int LENGTH_30 = 30;
    int LENGTH_40 = 40;
    int LENGTH_50 = 50;
    int LENGTH_100 = 100;

}
