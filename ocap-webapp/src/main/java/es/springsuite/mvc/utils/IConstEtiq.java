package es.springsuite.mvc.utils;

public interface IConstEtiq {
    String ETQ_ID_PROFESIONAL = "Id Profesional";
    String ETQ_ID_ESTATUTO = "Id Estatuto";
    String ETQ_NOMBRE = "Nombre";
    String ETQ_NOMBRE_CORTO = "Nombre corto";
    String ETQ_DESCRIPCION = "Descripción";
    String ETQ_FASE = "Fase";

    String ETQ_ID_MODALIDAD = "Id Modalidad";
    String ETQ_MODALIDAD = "Modalidad";
    
    String ETQ_ID_GRUPO_CATEGORIA = "Id Grupo Categoria";
    String ETQ_GRUPO_CATEGORIA = "Grupo Categoria";
   
    String ETQ_ID_GERENCIA = "Id Gerencia";
    String ETQ_GERENCIA = "Gerencia";
    String ETQ_COD_LDAP = "Cod. Ldap";
    

    String ETQ_TIPO_GERENCIA = "Tipo de Gerencia";
    
    String ETQ_PROVINCIA = "Provincia";
    
}
