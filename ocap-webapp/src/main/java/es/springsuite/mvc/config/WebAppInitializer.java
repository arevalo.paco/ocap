package es.springsuite.mvc.config;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
public class WebAppInitializer implements WebApplicationInitializer {
 
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        appContext.register(AppConfig.class);
        appContext.close();
// 
//        ContextLoaderListener contextLoaderListener = new ContextLoaderListener(
//                appContext);
//        servletContext.addListener(contextLoaderListener);
// 
//        FilterRegistration.Dynamic filter = servletContext.addFilter(
//                "StrutsDispatcher", new StrutsPrepareAndExecuteFilter());
//        filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
//                true, "/*");
// 
        }
 
}