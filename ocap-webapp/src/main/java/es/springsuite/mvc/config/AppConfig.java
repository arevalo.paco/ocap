package es.springsuite.mvc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@ComponentScans(value = { 
      @ComponentScan("org.springsuite.mvc") 
    })
public class AppConfig {
	
	@Autowired
	private
	LocalSessionFactoryBean sessionFactory;

	public LocalSessionFactoryBean getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(LocalSessionFactoryBean sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
