package es.springsuite.mvc.actions.catalogosaux;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import es.springsuite.mvc.actions.GenericAction;
import es.springsuite.mvc.beans.ot.catalogosaux.EspecialidadesOT;
import es.springsuite.mvc.entities.common.EspecialidadesEntity;
import es.springsuite.mvc.services.common.ITablasComunesService;
import es.springsuite.mvc.utils.IConstantesView;

@Controller("especialidadesAction")
public class EspecialidadesAction extends GenericAction {
    
    private static final long serialVersionUID = 7865238857580262049L;

    private Long cEspecialidadId;
    
    @Autowired
    private transient ITablasComunesService tablasComunesService;

    @GetMapping(value = "/irListado")
    @ModelAttribute(name = "model")
    public String irListado() {
        List<EspecialidadesEntity> listadoEspecialidades = tablasComunesService.getListaEspecialidades();
        getRequest().put(IConstantesView.ESPECIALIDADES_LISTADO, listadoEspecialidades);

        return SUCCESS;
    }

    /**
     * Procedimiento que carga los pantalla para una nueva especialidad
     * 
     * @return INPUT
     * 
     */
    public String irNuevo() {

        return INPUT;
    }

    /**
     * Procedimiento que carga los datos para la edición de las especialidades
     * 
     * @return INPUT
     */
    //@PostMapping(value = "/irEditar")
    public String irEditar() {
        EspecialidadesEntity entity = tablasComunesService.getEspecialidadById(getcEspecialidadId());

        getRequest().put(IConstantesView.ESPECIALIDADES, entity);
        return INPUT;
    }


    /**
     * Procedimiento que inserta o actualiza una especialidad
     * 
     * @param form model attribute especialidadesOT
     * @return INPUT
     */
    //@PostMapping(value = "/guardar")
    public String guardar(@ModelAttribute("especialidadesForm") EspecialidadesOT form) {
        try {
            EspecialidadesEntity entity = new EspecialidadesEntity();
            PropertyUtils.copyProperties(entity, form);
            tablasComunesService.guardarEspecialidad(entity);
            return SUCCESS;
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return ERROR;
        }
    }

    /**
     * Identificador de la especialidad
     * 
     * @return the cEspecialidadId
     */
    public Long getcEspecialidadId() {
        return cEspecialidadId;
    }

    /**
     * @param cEspecialidadId the cEspecialidadId to set
     */
    public void setcEspecialidadId(Long cEspecialidadId) {
        this.cEspecialidadId = cEspecialidadId;
    }

}
