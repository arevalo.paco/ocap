package es.springsuite.mvc.actions;

import java.util.ArrayList;
import java.util.List;

import es.springsuite.mvc.utils.ColumnBean;

public abstract class GenericRenderGrid {

    public List<ColumnBean> COLUMNAS  = new ArrayList<>();

    public String getColumnJson(ColumnBean column) {
        return  column.toJSONString();
    }
    
    public List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        for (int i=0; i<COLUMNAS.size(); i++) {
            columnNames.add("'" +  COLUMNAS.get(i).getNombre() + "'");
        }
        return columnNames;
    }
    
    public String generateGridModel() {
        
        StringBuilder strColModel = new StringBuilder();
        strColModel.append("[\r\n");
        for(int i=0; i<this.COLUMNAS.size(); i++) {
            ColumnBean columnBean = this.COLUMNAS.get(i);
            strColModel.append("{");
            strColModel.append("name: '").append(columnBean.getCampo()).append("', ");
            strColModel.append("align: '").append(columnBean.getAlineacion()).append("', ");
            strColModel.append("index: '").append(columnBean.getCampo()).append("', ");
            strColModel.append("width: '").append(columnBean.getAncho()).append("',");
            strColModel.append("sortable: '").append(columnBean.getOrdenable()).append("',");
            strColModel.append("editable: '").append(columnBean.getEditable()).append("',");
            strColModel.append("search: '").append(columnBean.getFiltrable()).append("',");
            strColModel.append("searchoptions: { sopt: ['cn'] }");
            strColModel.append("}")
            
            .append((i<this.COLUMNAS.size()-1 ? ",\r\n" : "\r\n"));
        }
        strColModel.append("]");
      
        return strColModel.toString();
    }


}
