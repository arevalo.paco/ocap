package es.springsuite.mvc.actions.catalogosaux;
 
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.opensymphony.xwork2.config.entities.Parameterizable;

import es.springsuite.mvc.actions.GenericAction;
import es.springsuite.mvc.beans.ot.catalogosaux.CategoriaProfesionalOT;
import es.springsuite.mvc.entities.common.CategoriasProfesionalesEntity;
import es.springsuite.mvc.entities.common.GruposCategoriasEntity;
import es.springsuite.mvc.entities.common.ModalidadesEntity;
import es.springsuite.mvc.entities.estatutos.EstatutarioEntity;
import es.springsuite.mvc.services.common.ITablasComunesService;
import es.springsuite.mvc.utils.IConstantesView;
import groovy.util.logging.Slf4j;
import lombok.Getter;
import lombok.Setter;

/**
 * Controlador para Categorias Profesionales
 * 
 * @author farevalo
 *
 */
@SuppressWarnings("serial")
@Controller("categProfesionalesAction")
@Slf4j
public class CategProfesionalesAction extends GenericAction implements Parameterizable {

    private static final String DATA_GID = "dataGrid";
    
    private static final String DATA_GRID_MODEL = "dataGridModel";
    
    private static final String DATA_GRID_COL_MODEL = "dataGridColModel";
    
    private static final String DATA_GRID_JSON = "dataGridJson";
    
    private static final String DATA_GRID_COL_NAMES = "dataGridColNames";
    
    /** Servicio */
    @Autowired
    private transient ITablasComunesService tablasComunesService;

    /** Formulario de edición de una categoria profesional*/
    @Getter @Setter
    private transient CategoriaProfesionalOT categoriasProfForm = new CategoriaProfesionalOT();
    
    /** Identificador del submin de categorias profesionales*/
    private Long cProfesionalId;

    @Autowired
    private transient CategProfesionalesRenderGrid renderGrid;
    
    /**
     * Listado de las categorias profesionales
     * 
     * @return
     * @throws IOException
     */
    public String irCategoriaProfesional() throws IOException {

        List<CategoriasProfesionalesEntity> listadoCategorias = tablasComunesService.getListaCategoriasProfesionales();
        List<EstatutarioEntity> listadoEstatutario = tablasComunesService.getListaEstatutos();
        
        ObjectMapper objMapper = new ObjectMapper(); 
        
        String strJson = objMapper.writeValueAsString(listadoCategorias);
        
        String strRenderGrid = generateRenderGrid(renderGrid, listadoCategorias);
        
        String strColModel = renderGrid.generateGridModel();
 
        getRequest().put(DATA_GRID_COL_MODEL, strColModel);
        getRequest().put(DATA_GRID_MODEL, StringEscapeUtils.escapeEcmaScript(strJson));
        getRequest().put(DATA_GRID_JSON, strRenderGrid);
        getRequest().put(DATA_GRID_COL_NAMES, renderGrid.getColumnNames().toString());
        getRequest().put(IConstantesView.ESTATUTOS_LISTADO, listadoEstatutario);
        
        return DATA_GID;
    }


    /**
     * Procedimiento que carga los pantalla para una nueva categoría profesional
     * 
     * @return INPUT
     */
    public String irNuevo() {

        return INPUT;
    }

    /**
     * Procedimiento que carga los datos para la edición de las categorias profesionales
     * 
     * @return INPUT
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     */
    public String irEditar() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        CategoriasProfesionalesEntity entity = 
                tablasComunesService.getCategoriasProfesionalesById(cProfesionalId);
        List<EstatutarioEntity> listadoEstatutario = tablasComunesService.getListaEstatutos();
        List<ModalidadesEntity> listadoModalidades = tablasComunesService.getListaModalidades();
        List<GruposCategoriasEntity> listadoGruposCategorias = tablasComunesService.getGruposCategorias();
        
        CategoriaProfesionalOT catOT = new CategoriaProfesionalOT();
        catOT.setdNombre(entity.getdNombre());
        PropertyUtils.copyProperties(catOT, entity);
        setCategoriasProfForm(catOT);

        getRequest().put(IConstantesView.ESTATUTOS_LISTADO, listadoEstatutario);
        getRequest().put(IConstantesView.MODALIDADES_LISTADO, listadoModalidades);
        getRequest().put(IConstantesView.GRUPO_CATEGORIAS_LISTADO, listadoGruposCategorias);
        getRequest().put("categoriaProfesionalOT", entity);
        getServletContext().setAttribute("catOT", entity);

        return INPUT;
    }


    /**
     * Procedimiento que inserta o actualiza una categoria profesional
     * 
     * @param form model attribute CategoriaProfesionalOT
     * @return INPUT
     */
    @PostMapping(value = "/guardarCategoriaProfesional")
    public String guardar(@ModelAttribute("categoriasProfForm") CategoriaProfesionalOT form) {
        try {
            CategoriasProfesionalesEntity entity = new CategoriasProfesionalesEntity();
            PropertyUtils.copyProperties(entity, form);
            tablasComunesService.guardarCategProfesional(entity);
            return SUCCESS;
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return ERROR;
        }
    }

    /**
     * Id seleccionado de la categoria profesional
     * 
     * @return
     */
    public Long getCProfesionalId() {
        return cProfesionalId;
    }


    public void setCProfesionalId(Long cProfesionalId) {
        this.cProfesionalId = cProfesionalId;
    }


    /**
     * @return the categoriasProfForm
     */
    public CategoriaProfesionalOT getCategoriasProfForm() {
        return categoriasProfForm;
    }


    /**
     * @param categoriasProfForm the categoriasProfForm to set
     */
    public void setCategoriasProfForm(CategoriaProfesionalOT categoriasProfForm) {
        this.categoriasProfForm = categoriasProfForm;
    }
    

}