package es.springsuite.mvc.actions.catalogosaux;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import es.springsuite.mvc.actions.GenericRenderGrid;
import es.springsuite.mvc.utils.ColumnBean;
import es.springsuite.mvc.utils.IConstEnt;
import es.springsuite.mvc.utils.IConstEtiq;
import es.springsuite.mvc.utils.IConstantesView;

@Component
public class CategProfesionalesRenderGrid extends GenericRenderGrid {

    public CategProfesionalesRenderGrid() {
        COLUMNAS = new ArrayList<>();
        
        COLUMNAS.add(new ColumnBean(IConstEnt.C_PROFESIONAL_ID, IConstEtiq.ETQ_ID_PROFESIONAL, IConstantesView.LENGTH_5, true, true,
                false, ColumnBean.align.DRCHA));

        COLUMNAS.add(new ColumnBean(IConstEnt.CESTATUDID, IConstEtiq.ETQ_ID_ESTATUTO, IConstantesView.LENGTH_5, true, true, false,
                ColumnBean.align.DRCHA));

        COLUMNAS.add(new ColumnBean(IConstEnt.D_NOMBRE, IConstEtiq.ETQ_NOMBRE, IConstantesView.LENGTH_20, true, true, false,
                ColumnBean.align.IZDA));

        COLUMNAS.add(new ColumnBean(IConstEnt.C_MODALIDAD_ID, IConstEtiq.ETQ_ID_MODALIDAD, IConstantesView.LENGTH_5, true, true, false,
                ColumnBean.align.DRCHA));

        COLUMNAS.add(new ColumnBean(IConstEnt.MODALIDAD_ENTITY + "." + IConstEnt.D_NOMBRE, IConstEtiq.ETQ_MODALIDAD, IConstantesView.LENGTH_5, true, true, false,
                ColumnBean.align.IZDA));

        COLUMNAS.add(new ColumnBean(IConstEnt.C_GRUPOCATEGORIA_ID, IConstEtiq.ETQ_ID_GRUPO_CATEGORIA, IConstantesView.LENGTH_5, true, true, false,
                ColumnBean.align.IZDA));

        COLUMNAS.add(new ColumnBean(IConstEnt.GRUPO_CATEGORIAS_ENTITY + "." + IConstEnt.D_NOMBRE, IConstEtiq.ETQ_GRUPO_CATEGORIA, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));

    }


}
