package es.springsuite.mvc.actions.catalogosaux;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import es.springsuite.mvc.actions.GenericAction;
import es.springsuite.mvc.entities.gerencias.GerenciasEntity;
import es.springsuite.mvc.services.common.IGerenciasService;
import groovy.util.logging.Slf4j;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Controller("gerenciasAction")
@Slf4j
public class GerenciasAction extends GenericAction {

    private static final String DATA_GID = "dataGrid";
    
    private static final String DATA_GRID_MODEL = "dataGridModel";
    
    private static final String DATA_GRID_COL_MODEL = "dataGridColModel";
    
    private static final String DATA_GRID_JSON = "dataGridJson";
    
    private static final String DATA_GRID_COL_NAMES = "dataGridColNames";
    
    /** Servicio */
    @Autowired
    private transient IGerenciasService gerenciasService;

    @Autowired
    private transient GerenciasRenderGrid renderGrid;
    
    @Getter @Setter
    private Long cGerenciaId;
    
//    @GetMapping(value = "/irListado")
    @ModelAttribute(name = "model")
    public String irListado() throws IOException {
        List<GerenciasEntity> listadoGerencias = gerenciasService.listarGerencias();
        getRequest().put("listadoGerencias", listadoGerencias);
        
        ObjectMapper objMapper = new ObjectMapper(); 
        
        String strJson = objMapper.writeValueAsString(listadoGerencias);

        String strRenderGrid = generateRenderGrid(renderGrid, listadoGerencias);
        
        String strColModel = renderGrid.generateGridModel();
        
        getRequest().put(DATA_GRID_COL_MODEL, strColModel);
        getRequest().put(DATA_GRID_MODEL, StringEscapeUtils.escapeEcmaScript(strJson));
        getRequest().put(DATA_GRID_JSON, strRenderGrid);
        getRequest().put(DATA_GRID_COL_NAMES, renderGrid.getColumnNames().toString());
        
        return DATA_GID;

    }

    /**
     * Procedimiento que carga los datos para la edición de las gerencias
     * 
     * @return INPUT
     */
  //  @PostMapping(value = "/irEditar")
    public String irEditar() {
         GerenciasEntity entity = 
                gerenciasService.getGerenciaById(cGerenciaId);
        getRequest().put("gerenciaOT", entity);
        return INPUT;
    }


}
