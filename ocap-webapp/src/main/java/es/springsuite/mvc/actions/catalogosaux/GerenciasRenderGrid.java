package es.springsuite.mvc.actions.catalogosaux;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import es.springsuite.mvc.actions.GenericRenderGrid;
import es.springsuite.mvc.utils.ColumnBean;
import es.springsuite.mvc.utils.IConstEnt;
import es.springsuite.mvc.utils.IConstEtiq;
import es.springsuite.mvc.utils.IConstantesView;

@Component
public class GerenciasRenderGrid extends GenericRenderGrid {

    public GerenciasRenderGrid() {
        COLUMNAS = new ArrayList<>();

        COLUMNAS.add(new ColumnBean(IConstEnt.C_GERENCIA_ID, IConstEtiq.ETQ_ID_GERENCIA, IConstantesView.LENGTH_5, true, true,
                false, ColumnBean.align.CENTRAR));

        COLUMNAS.add(new ColumnBean(IConstEnt.PROVINCIAS_ENTITY + "." + IConstEnt.C_PROVINCIA_ID, IConstEtiq.ETQ_PROVINCIA, IConstantesView.LENGTH_5, true, true, false,
                ColumnBean.align.DRCHA));

        COLUMNAS.add(new ColumnBean(IConstEnt.PROVINCIAS_ENTITY + "." + IConstEnt.D_PROVINCIA, IConstEtiq.ETQ_PROVINCIA, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));

        COLUMNAS.add(new ColumnBean(IConstEnt.TIPO_GERENCIAS_ENTITY + "."  + IConstEnt.D_NOMBRE , IConstEtiq.ETQ_TIPO_GERENCIA, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));
        
        COLUMNAS.add(new ColumnBean(IConstEnt.D_NOMBRE_CORTO, IConstEtiq.ETQ_NOMBRE_CORTO, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));
        

        COLUMNAS.add(new ColumnBean(IConstEnt.D_NOMBRE, IConstEtiq.ETQ_NOMBRE, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));
    }
}
