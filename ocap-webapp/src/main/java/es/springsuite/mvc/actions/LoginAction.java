package es.springsuite.mvc.actions;

import javax.transaction.Transactional;

import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.springsuite.mvc.beans.ot.login.LoginUserOT;
import es.springsuite.mvc.entities.usuarios.UsuariosEntity;
import es.springsuite.mvc.services.usuarios.IUsuariosService;
import lombok.Getter;
import lombok.Setter;

/**
 * Controller for login
 * 
 * @author farevalo
 *
 */
@SuppressWarnings("serial")
@Controller("loginAction")
public class LoginAction extends DefaultActionSupport {

	@Autowired
	private IUsuariosService usuariosService;

	@Getter @Setter
	private LoginUserOT loginUserOT = new LoginUserOT();
	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	@Transactional
    public String login() {
		UsuariosEntity usuario = usuariosService.login(getLoginUserOT().username, getLoginUserOT().password);
		if (usuario != null) {
            return SUCCESS;
        } else {
            return ERROR;
        }
	}
	
	public String irLogin() {
		return INPUT;
	
	}

    public String index() {
        return INPUT;
    
    }

	@RequestMapping(value="/irBienvenida", method = RequestMethod.GET)
	public String irBienvenida() {
		return SUCCESS;
	}

	/**
	 * @return the loginUserOT
	 */
	public LoginUserOT getLoginUserOT() {
		return loginUserOT;
	}


	/**
	 * @param loginUserOT the loginUserOT to set
	 */
	public void setLoginUserOT(LoginUserOT loginUserOT) {
		this.loginUserOT = loginUserOT;
	}
}
