package es.springsuite.mvc.actions.catalogosaux;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import es.springsuite.mvc.actions.GenericAction;
import es.springsuite.mvc.beans.ot.catalogosaux.CategoriaProfesionalOT;
import es.springsuite.mvc.entities.common.CategoriasProfesionalesEntity;
import es.springsuite.mvc.entities.common.EstadosEntity;
import es.springsuite.mvc.services.common.ITablasComunesService;
import groovy.util.logging.Slf4j;
import lombok.Getter;
import lombok.Setter;

/**
 * Controlador para los Estadoos
 * 
 * @author farevalo
 *
 */
@SuppressWarnings("serial")
@Controller("estadosAction")
@Slf4j
public class EstadosAction extends GenericAction {

    private static final String DATA_GID = "dataGrid";
    
    private static final String DATA_GRID_MODEL = "dataGridModel";
    
    private static final String DATA_GRID_COL_MODEL = "dataGridColModel";
    
    private static final String DATA_GRID_JSON = "dataGridJson";
    
    private static final String DATA_GRID_COL_NAMES = "dataGridColNames";
    
    /** Servicio */
    @Autowired
    private transient ITablasComunesService tablasComunesService;

    /** Formulario de edición de una categoria profesional*/
    @Getter @Setter
    private transient CategoriaProfesionalOT categoriasProfForm = new CategoriaProfesionalOT();
    
    /** Identificador del submin de categorias profesionales*/
    private Long cProfesionalId;

    @Autowired
    private transient EstadosRenderGrid renderGrid;
    
    @GetMapping(value = "/irListadoGridEstados")
    @ModelAttribute(name = "model")
    public String irListado() throws IOException {
        
        
        List<EstadosEntity> listadoEstados = tablasComunesService.getListaEstados();


        ObjectMapper objMapper = new ObjectMapper(); 
        
        String strJson = objMapper.writeValueAsString(listadoEstados);

        String strRenderGrid = generateRenderGrid(renderGrid, listadoEstados);

        String strColModel = renderGrid.generateGridModel();
        getRequest().put(DATA_GRID_COL_MODEL, strColModel);
        getRequest().put(DATA_GRID_MODEL, StringEscapeUtils.escapeEcmaScript(strJson));
        getRequest().put(DATA_GRID_JSON, strRenderGrid);
        getRequest().put(DATA_GRID_COL_NAMES, renderGrid.getColumnNames().toString());
        

        return DATA_GID;
    }


    /**
     * Procedimiento que carga los pantalla para una nueva categoría profesional
     * 
     * @return INPUT
     */
    public String irNuevo() {

        return INPUT;
    }

    /**
     * Procedimiento que carga los datos para la edición de las categorias profesionales
     * 
     * @return INPUT
     */
    @PostMapping(value = "/irEditarEstados")
    public String irEditarEstados() {
         CategoriasProfesionalesEntity entity = 
                tablasComunesService.getCategoriasProfesionalesById(getCProfesionalId());
        getRequest().put("categoriaProfesionalOT", entity);
        return INPUT;
    }


    /**
     * Id seleccionado de la categoria profesional
     * 
     * @return
     */
    public Long getCProfesionalId() {
        return cProfesionalId;
    }


    public void setCProfesionalId(Long cProfesionalId) {
        this.cProfesionalId = cProfesionalId;
    }


    /**
     * @return the categoriasProfForm
     */
    public CategoriaProfesionalOT getCategoriasProfForm() {
        return categoriasProfForm;
    }


    /**
     * @param categoriasProfForm the categoriasProfForm to set
     */
    public void setCategoriasProfForm(CategoriaProfesionalOT categoriasProfForm) {
        this.categoriasProfForm = categoriasProfForm;
    }
    
}