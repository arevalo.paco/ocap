package es.springsuite.mvc.actions.catalogosaux;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import es.springsuite.mvc.actions.GenericRenderGrid;
import es.springsuite.mvc.utils.ColumnBean;
import es.springsuite.mvc.utils.IConstEnt;
import es.springsuite.mvc.utils.IConstEtiq;
import es.springsuite.mvc.utils.IConstantesView;

@Component
public class EstadosRenderGrid extends GenericRenderGrid {

    public EstadosRenderGrid() {
        COLUMNAS = new ArrayList<>();

        COLUMNAS.add(new ColumnBean(IConstEnt.C_ESTADO_ID, IConstEtiq.ETQ_ID_ESTATUTO, IConstantesView.LENGTH_5, true, true,
                false, ColumnBean.align.CENTRAR));

        COLUMNAS.add(new ColumnBean(IConstEnt.D_NOMBRE, IConstEtiq.ETQ_NOMBRE, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));

        COLUMNAS.add(new ColumnBean(IConstEnt.DDESCRIPCION, IConstEtiq.ETQ_DESCRIPCION, IConstantesView.LENGTH_20, true, true, false,
                ColumnBean.align.IZDA));
        
        COLUMNAS.add(new ColumnBean(IConstEnt.C_FASE, IConstEtiq.ETQ_FASE, IConstantesView.LENGTH_10, true, true, false,
                ColumnBean.align.IZDA));
    }
}
