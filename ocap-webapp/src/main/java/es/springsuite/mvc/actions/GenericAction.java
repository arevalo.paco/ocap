package es.springsuite.mvc.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.apache.struts2.interceptor.CookiesAware;
import org.apache.struts2.interceptor.RequestAware;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opensymphony.webwork.interceptor.ApplicationAware;
import com.opensymphony.webwork.interceptor.PrincipalAware;
import com.opensymphony.webwork.interceptor.PrincipalProxy;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import com.opensymphony.webwork.interceptor.ServletResponseAware;
import com.opensymphony.webwork.interceptor.SessionAware;
import com.opensymphony.webwork.util.ServletContextAware;
import com.opensymphony.xwork2.config.entities.Parameterizable;

import es.springsuite.mvc.utils.ColumnBean;


/**
 * Controlador para Categorias Profesionales
 * 
 * @author farevalo
 *
 */
@SuppressWarnings({"serial","unused"})
@Controller("categProfesionalesAction")
public class GenericAction extends DefaultActionSupport 
implements ApplicationAware, CookiesAware, SessionAware, RequestAware, ServletRequestAware,
ServletResponseAware, ServletContextAware , PrincipalAware, Parameterizable {

	// variables for *Aware interfaces
    
	private transient HttpServletRequest request = null;
	
	private transient HttpServletResponse response = null;
	private transient Map<String, Object> requestAttributes = null;
	private transient Map<String, Object> sessionAttributes = null;
	private transient PrincipalProxy principalProxy = null;
	private transient Map<String, Object> contextAttributes = null;
	private transient Map<String, String> requestCookies = null;
	private transient Map<?,?> application = null;
	private transient Map<String, String> params = new HashMap<>();
	private transient ServletContext servletContext = null;
	
	@Override
    public void setServletContext(ServletContext context) {
        this.servletContext = context;
    } 
	
	public ServletContext getServletContext() {
	    return this.servletContext;
	}
	
	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request; 
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		this.requestAttributes  = request;
	}

	public Map<String, Object> getRequest() {
		return this.requestAttributes;
	}
	
	public HttpServletResponse getResponse() {
		return this.response;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void setSession(Map session) {
		this.sessionAttributes = session;
	}
	
	public Map<String, Object> getSession(){
		return sessionAttributes ;
	}

	@Override
	public void setPrincipalProxy(PrincipalProxy principalProxy) {
		this.principalProxy = principalProxy;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void setCookiesMap(Map<String, String> cookies) {
		this.requestCookies = cookies;
	}


    /**
     * {@inheritDoc}
     */
	@SuppressWarnings("rawtypes")
	@Override
	public void setApplication(Map application) {
		this.application = application;
	}

    /**
     * Procedimiento que genera el grid
     * @param <T>
     * @param strJson
     * @return
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonGenerationException 
     */
    protected <T> String generateRenderGrid(GenericRenderGrid genericRenderGrid, List<T> datos) throws JsonGenerationException, JsonMappingException, IOException {
               
        ObjectMapper objMapper = new ObjectMapper(); 
        
        objMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        
        String strJson = objMapper.writeValueAsString(datos);

        List<ColumnBean> columnasAux = genericRenderGrid.COLUMNAS;
        StringBuilder colNames = new StringBuilder();
        StringBuilder colModel = new StringBuilder();
        for (int i=0; i< columnasAux.size(); i++) {
            colModel.append(genericRenderGrid.getColumnJson(columnasAux.get(i)));
            colNames.append( "'" + columnasAux.get(i).getNombre() + "'");
            if (i<columnasAux.size()-1) {
                colModel.append(",");
                colNames.append(",");
            }
        }
        
        
        StringBuilder renderG = new StringBuilder();

        renderG.append("{")
        .append("colNames: [").append(colNames).append("],")
        .append("colModel: [").append(colModel).append("],")
        .append("rowNum:10, ")
        .append("rowList:[5,10,20,30],") 
        .append("width: 1000,")
        .append("rownumbers:true,")
        .append("viewrecords:true, ")
        .append("data: " + strJson + ",")
        .append("pager: '#pgGrid'") 
      
        .append("}");
    

        
        return renderG.toString();
    }

    @Override
    public void addParam(String name, String value) {
        this.params.put(name, value);
    }

    @Override
    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public Map<String, String> getParams() {
        return this.params;
    }
    
}