package es.springsuite.mvc.actions.rest.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactApiRestController {

    @RequestMapping(value="/api/holaMundo", method=RequestMethod.GET)
    public String holaMundo() {
        return "hola Mundo!!";
    }
}
