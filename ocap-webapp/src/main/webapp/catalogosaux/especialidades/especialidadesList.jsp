<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<%@ taglib prefix="logic" uri="/struts-logic.tld" %>
<%@ taglib prefix="bean" uri="/struts-bean.tld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
	<style type="text/css">
		.headerTable {
			font-family: arial;
			font-size: 14px;
			background-color: #D35400;
		}
		.celda1 {
			font-family: arial;
			font-size: 14px;
		}
		.celda2 {
			font-family: arial;
			font-size: 14px;
			background-color: #DC8145;
		}
		.celda1:hover {
			cursor: pointer;
		}
		.celda2:hover {
			cursor: pointer;
		}
	</style>
	<script type="text/javascript">
		function highlightNext(element, color) {
		    var next = element;
		    do { // find next td node
		        next = next.nextSibling;
		    }
		    while (next && !('nodeName' in next && next.nodeName === 'TD'));
		    if (next) {
		        next.style.color = color;
		    }
		}
	
		function highlightBG(element, color) {
		    element.style.backgroundColor = color;

		    var next = element;
		    do { // find next td node
		        next = next.nextSibling;
		    }
		    while (next && !('nodeName' in next && next.nodeName === 'TD'));
		    if (next) {
		        next.style.backgroundColor = color;
		    }
		}

		function nuevo() {
			alert('nuevo');
		}

		function irEditar(idCategoria) {
			document.forms['categoriasProfForm'].elements['cProfesionalId'].value=idCategoria;
			document.forms['categoriasProfForm'].submit();
		}
	</script>
	
</head>
<body>

    <div align="center" style="height: 490px; overflow-y: scroll;">
        <s:form action="especialidadesList.action" method="post">
		  <div style="background-color: #aa0000; width: 100%">
				<s:a cssStyle="float: right" onclick="javascript:nuevo()">
					<img alt="nuevo" src="http://localhost:8080/ocap/img/nuevo.gif" width="32" height="32">
				</s:a>		  		
		  </div>
          <div>
        	<table style="width: 100%">
				<thead>
					<tr class="headerTable"	>
						<td width="1%">Id</td>
						<td width="49%">Cod. Categoria</td>
						<td width="10%">Categoria</td>
		 				<td width="40%">Especialidad</td>
					</tr>
				</thead>
				<tbody>
				<% long count = 1;%>
					<logic:iterate id="lista" name="listadoEspecialidades" indexId="currentIndex">
						 
						 <% if (count % 2 != 0) {%>
							<tr onMouseOver="highlightBG(this, '#ccc')" 
    							onMouseOut="highlightBG(this, '#FAD7A0')"
    							onclick="irEditar('<bean:write name="lista" property="cProfesionalId"/>')">
								<td class="celda1"><%=count %></td>
								<td class="celda1"><bean:write name="lista" property="dNombre"/></td>
								<td class="celda1"><bean:write name="lista" property="categoriasProfesional.cEstatutId"/></td>
								<td class="celda1"><bean:write name="lista" property="categoriasProfesional.dNombre"/></td>
							</tr>
						<%} %>
						<% if (count % 2 == 0) {%>
						
							<tr onMouseOver="highlightBG(this, '#ccc')" 
    							onMouseOut="highlightBG(this, '#FF0000')"
    							onclick="irEditar('<bean:write name="lista" property="cProfesionalId"/>')">
								<td class="celda2"><%=count %></td>
								<td class="celda2"><bean:write name="lista" property="dNombre"/></td>
								<td class="celda2"><bean:write name="lista" property="categoriasProfesional.cEstatutId"/></td>
								<td class="celda2"><bean:write name="lista" property="categoriasProfesional.dNombre"/></td>
							</tr>
						<%} %>
						<% count++; %>

					</logic:iterate>
					
				</tbody>
			</table>       
		  </div> 
        </s:form>            
        <s:form name="especialidadesForm" modelAttribute="especialidadesForm"  action="irEditarEspecialidades.action" method="POST">
        	<s:hidden name="cProfesionalId" />
        </s:form>
    </div>   
</body>
</html>