<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="bean" uri="/struts-bean.tld" %>

<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Your page title</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.5/css/ui.jqgrid.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.5/jquery.jqgrid.min.js"></script>
    
</head>
<body>         
	<div style="margin-top: 2em; margin-left: 2em;">
	<table id="grid"></table>
	<div id="pgGrid"></div>
	
</div>
	<script type="text/javascript">	
	$(function () {
	    $("#grid").jqGrid({
		    colNames: <%=request.getAttribute("dataGridColNames")%>,
	        colModel: <%=request.getAttribute("dataGridColModel")%>,
	        rowNum:10, 
	        rowList:[5,10,15], 
	        width: 1000,
	        rownumbers:true,
	        viewrecords:true,
	        autosearch: true, 
			data:   $.parseJSON('<%=request.getAttribute("dataGridModel")%>'),
			pager: '#pgGrid',
			onSelectRow : function(id) {
				 var rid = jQuery('#grid').jqGrid("getGridParam", "selrow");
                 if (rid) {
	                var row = jQuery('#grid').jqGrid("getRowData", rid);
	                alert(row.cProfesionalId+", "+row.cEstadoId+", "+row.dNombre);
	             }
			}
				        
	    });

	    $("#grid").jqGrid('filterToolbar', { 
		      autosearch: true,
	    	  stringResult: true,
	    	  searchOnEnter: false,
	    	  defaultSearch: "cn"
		 });
		
		jQuery("#grid").jqGrid('navGrid','#pgGrid',{del:false,add:false,edit:false});
	    		
	});	

	</script>	
</body>
</html>