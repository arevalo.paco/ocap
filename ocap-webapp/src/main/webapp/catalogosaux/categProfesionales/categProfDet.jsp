<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="logic" uri="/struts-logic.tld" %>
<%@ taglib prefix="bean" uri="/struts-bean.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
      <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      

      <!-- CSS -->
      <style>
         .ui-widget-header,.ui-state-default, ui-button {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
      
		  #feedback { font-size: 1.4em; }
		  #selectable .ui-selecting { background: #FECA40; }
		  #selectable .ui-selected { background: #F39814; color: white; }
		  #selectable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		  #selectable li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }
		  a {
		  	text-decoration: none;
		  }
		  
		  input#dDescripcion {
		  	width: 20em;
		  }
	  </style>
      
      <!-- Javascript -->
      <script>
         var dialog;
         $(function() {

            $( "#btnBuscarEstatuto" ).click(function() {
            	dialog = $( "#dlgEstatuto" ).dialog({
                    autoOpen: true, 
                    buttons: {
                       OK: function() {
                    	   $("#dlgEstatuto").dialog("close");
                       }
                    },
                    title: "Estatutos",
                    position: {
                       my: "center center",
                       at: "center center"
                    }
                 });
            });

            $( "#btnBuscarModalidad" ).click(function() {
            	dialog = $( "#dlgEstatuto" ).dialog({
                    autoOpen: true, 
                    buttons: {
                       OK: function() {
                    	   $("#dlgModalidades").dialog("close");
                       }
                    },
                    title: "Modalidades",
                    position: {
                       my: "center center",
                       at: "center center"
                    }
                 });
            });
         });

         
      </script>      
</head>
<body>
	<s:set name="catOT" value="categoriasProfForm"/>
	<form:form modelAttribute="command" >
		
	    <div class="centar2">
	        <h1>Categoria Profesional</h1>
		         <div>	
		           <div class="label1">Estatuto:</div>
		           <div class="caja1" style="display: inline;">
		           <s:hidden name="cEstatutId" value="%{#catOT.getcEstatutId()}"/>
		           <s:textfield name="dNombre" cssClass="textoLargo" value="%{#catOT.getdNombre()}"></s:textfield>
		           <div id="btnBuscarEstatuto" style="border: 1px #000000 solid; width: 2em; display: inline; cursor: pointer;">
		           ...</div>
		           </div> 
		         </div>
		         <div class="separador"></div> 
		         <div>
		            <div class="label1">Profesional:</div>
		            <div class="caja1"><s:textfield name="%{#catOT.getcProfesionalId()}" cssClass="texto"/></div> 
		         </div>
		         <div class="separador"></div> 
		         <div>
		            <div class="label1">Descripción:</div>
		            <div class="caja1"><s:textfield name="dDescripcion"  cssClass="textoLargo" value="%{#catOT.getdDescripcion()}"></s:textfield></div> 
		         </div>
		         <div class="separador"></div> 
		         <div>	
		           <div class="label1">Modalidad:</div>
		           <div class="caja1" style="display: inline;">
		           <s:hidden name="cModalidadId" value="%{#catOT.getModalidadesEntity().getcModalidadId()}"/>
		           <s:textfield name="dModalidadNombre" cssClass="textoLargo" value="%{#catOT.getModalidadesEntity().getdNombre()}"></s:textfield>
		           <div id="btnBuscarModalidad" style="border: 1px #000000 solid; width: 2em; display: inline; cursor: pointer;">
		           ...</div>
		           </div> 
		         </div>
		         <div class="separador"></div> 
		         <div>	
		           <div class="label1">Grupo Catg:</div>
		           <div class="caja1" style="display: inline;">
		           <s:hidden name="cGrupoCategoriaId" value="%{#catOT.getGruposCategoriasEntity().getcGrupoCategoriaId()}"/>
		           <s:textfield name="dNombreCategoria" cssClass="textoLargo" value="%{#catOT.getGruposCategoriasEntity().getdNombre()}"></s:textfield>
		           <div id="btnBuscarCategoria" style="border: 1px #000000 solid; width: 2em; display: inline; cursor: pointer;">
		           ...</div>
		           </div> 
		         </div>
		         <div class="separador"></div> 
		         <div style="display: block; text-align: center">
		         	<s:submit value="Guardar" title="Guardar" />
	        	</div>
	    </div>   
            	
    </form:form>
     <div id = "dlgEstatuto" title = "Estatuto" >
     	<div style="height: 200px; overflow-y: scroll, text-align: left;">
			
			<logic:iterate id="lista" name="listadoEstatutos" indexId="currentIndex">
			 -	<a href="javascript:cerrarDlgEstatuto('<bean:write name="lista" property="cEstatutIdS"/>','<bean:write name="lista" property="dNombre"/>');"> 
			 	<bean:write name="lista" property="dNombre"/>
			 	</a><br/>
			</logic:iterate>
		
		</div>
	</div>
	<div id = "dlgModalidades" title = "Modalidades" >
     	<div style="height: 200px; overflow-y: scroll, text-align: left;">

			<logic:iterate id="listaMod" name="listadoModalidades" indexId="currentIndex">
			 -	<a href="javascript:cerrarDlgModalidad('<bean:write name="listaMod" property="cModalidadIdToString"/>','<bean:write name="listaMod" property="dNombre"/>');"> 
			 	<bean:write name="listaMod" property="dNombre"/>
			 	</a><br/>
			</logic:iterate>
		</div>
	</div>
    <script type="text/javascript">
    $( "#dlgEstatuto" ).dialog({
        autoOpen: false, 
        buttons: {
           OK: function() {$(this).dialog("close");}
        },
        title: "Estatutos",
        position: {
           my: "center center",
           at: "center center"
        }
     });
    $( "#dlgModalidades").dialog({
        autoOpen: false, 
        buttons: {
           OK: function() {$(this).dialog("close");}
        },
        title: "Modalidades",
        position: {
           my: "center center",
           at: "center center"
        }
     });

    function cerrarDlgEstatuto(id, descripcion) {
    	$("#dlgEstatuto").dialog("close");
    	$("#cEstatutId").val(id);
    	$("#dNombre").val(descripcion);
    }

    function cerrarDlgModalidad(id, descripcion) {
    	$("#dlgModalidades").dialog("close");
    	$("#cModalidadId").val(id);
    	$("#dModalidadNombre").val(descripcion);
    }
    </script>
	
</body>
</html>