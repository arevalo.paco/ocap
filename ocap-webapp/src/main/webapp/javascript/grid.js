function highlightNext(element, color) {
	var next = element;
	do { // find next td node
		next = next.nextSibling;
	}
	while (next && !('nodeName' in next && next.nodeName === 'TD'));
	if (next) {
		next.style.color = color;
	}
}

function highlightBG(element, color) {
	element.style.backgroundColor = color;

	var next = element;
	do { // find next td node
		next = next.nextSibling;
	}
	while (next && !('nodeName' in next && next.nodeName === 'TD'));
	if (next) {
		next.style.backgroundColor = color;
	}
}

