<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Spring and Struts Integration Demo</title>
	<link rel="stylesheet" href="css/estilos.css"/>
</head>
<body>
    <form:form modelAttribute="command" action="login.action" method="post">
    <div class="centar">
        <h1>Obtención de carrera profesional</h1>
        <div class = "marco">
	        <h2>Login</h2>
	         <div>	
	           <div class="label1">Usuario:</div>
	           <div class="caja1"><s:textfield name="loginUserOT.username" cssClass="texto"/> </div> 
	         </div>
	         <div class="separador"></div> 
	         <div>
	            <div class="label1">Password:</div>
	            <div class="caja1"><s:password name="loginUserOT.password" cssClass="texto"/> </div> 
	         </div>
	         <div class="separador"></div> 
	         <div style="display: block; text-align: center">
	         	<s:submit value="Login" title="Login" />
        	</div>
    	</div>
    </div>   
    </form:form>
</body>
</html>