	<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Spring and Struts Integration Demo</title>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/estilos.css"/>
</head>
<body>
    <div class="centar">
		<h1>Obtención de carrera profesional</h1>
		<div class = "marco2">
	        <div class="vertical-menu">
			  <a href="#" class="active">Catálogos</a>
			  <a href="<%=request.getContextPath()%>/irCategoriaProfesional.action">C. Profesionales</a>
              <a href="<%=request.getContextPath()%>/irEspecialidades.action">Especialidades</a>
			  <a href="<%=request.getContextPath()%>/irEstados.action">Estados</a>
			  <a href="#">Modalidades</a>
			  <a href="<%=request.getContextPath()%>/irGerencias.action">Gerencias</a>
			  
			</div>
			<div class="marcoBody">
        		<tiles:insertAttribute name="body"/>
        	</div>
        </div>   
    </div>   
</body>
</html>